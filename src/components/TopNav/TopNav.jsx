import React, { Component } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "./TopNav.css";

class TopNav extends Component {
  render() {
    return (
      <div className='topnav'>
        <div>
          <span className='bars-span' onClick={this.props.openSidebar}>
            <FontAwesomeIcon className='bars' icon='bars' />
          </span>
        </div>
        <p className='logo' onClick={this.props.openSidebar}>
          martwa crova
        </p>
      </div>
    );
  }
}

export default TopNav;
