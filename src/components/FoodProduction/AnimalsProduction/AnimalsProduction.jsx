import React, { Component } from "react";
import SelectAnimals from "../../SelectAnimals/SelectAnimals";
import LiveCounter from "../../LiveCounter/LiveCounter";
import { Line } from "react-chartjs-2";

class AnimalsProduction extends Component {
  constructor(props) {
    super(props);
    this.state = {
      animals: [
        {
          id: 4453,
          value: "all",
          text: "Ogółem",
          selected: true,
          govData: null
        },
        {
          id: 4454,
          value: "beef",
          text: "Wołowina",
          selected: false,
          govData: null
        },
        {
          id: 4455,
          value: "veal",
          text: "Cielęcina",
          selected: false,
          govData: null
        },
        {
          id: 4456,
          value: "pork",
          text: "Wieprzowina",
          selected: false,
          govData: null
        },
        {
          id: 4457,
          value: "mutton",
          text: "Baranina",
          selected: false,
          govData: null
        },
        {
          id: 4458,
          value: "poultry",
          text: "Drób",
          selected: false,
          govData: null
        }
      ],
      dataChart: {
        labels: [],
        datasets: [
          {
            label: "ton",
            backgroundColor: "rgb(99, 99, 99)",
            borderColor: "rgb(199, 199, 199)",
            data: []
          }
        ]
      },
      optionsChart: {
        scales: {
          yAxes: [
            {
              ticks: {
                min: 0
              }
            }
          ]
        }
      }
    };
  }

  componentDidMount() {
    this.setGovData().then(() => this.setChart());
  }

  setGovData = async () => {
    let actions = this.state.animals.map(x => x.id).map(this.apiRequest);
    await Promise.all(actions).then(data =>
      data.forEach(govItem => {
        if (govItem) {
          this.setState(prevState => {
            let animal = prevState.animals.find(
              x => x.id === govItem.variableId
            );
            animal.govData = govItem;
            return prevState;
          });
        }
      })
    );
  };

  setChart = () => {
    let values = this.actualValues();
    this.setState(prevState => {
      prevState.dataChart.labels = values.map(x => x.year);
      prevState.dataChart.datasets[0].data = values.map(x => x.val);

      return prevState;
    });
  };

  apiRequest = id =>
    fetch(`https://bdl.stat.gov.pl/api/v1/data/by-variable/${id}`)
      .then(response => response.json())
      .catch(e => console.log(e));

  handleOnSelected = selected => {
    this.setState(prevState => {
      prevState.animals.find(
        animal => animal.selected === true
      ).selected = false;
      prevState.animals.find(
        animal => animal.value === selected
      ).selected = true;
      return prevState;
    }, this.setChart);
  };

  actualValues = () => {
    let govData = this.state.animals.find(animal => animal.selected).govData;
    if (!govData) {
      return [];
    }
    return govData.results.find(r => r.id === "000000000000").values;
  };

  actualState = () => {
    let values = this.actualValues();
    if (values.length <= 0) {
      return 0;
    }
    return values[values.length - 1];
  };

  render() {
    return (
      <>
        <h4 className='population-header'>Produkcja żywca zwierzęcego</h4>
        <h6 className='population-info'>
          Ile kilogramów mięsa produkują polscy rolnicy?
        </h6>
        <SelectAnimals
          items={this.state.animals}
          onSelected={this.handleOnSelected}
        />
        <p>{this.actualState().year}</p>
        <LiveCounter
          unit='kg'
          zeros='000'
          totalCount={this.actualState().val}
        />
        <div className='chart-container'>
          <Line data={this.state.dataChart} options={this.state.optionsChart} />
        </div>
      </>
    );
  }
}

export default AnimalsProduction;
