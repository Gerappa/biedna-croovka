import React, { Component } from "react";
import LiveCounter from "../../LiveCounter/LiveCounter";
import { Line } from "react-chartjs-2";

class EggsProduction extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: 4467,
      govData: null,

      dataChart: {
        labels: [],
        datasets: [
          {
            label: "tys. sztuk",
            backgroundColor: "rgb(99, 99, 99)",
            borderColor: "rgb(199, 199, 199)",
            data: []
          }
        ]
      },
      optionsChart: {
        scales: {
          yAxes: [
            {
              ticks: {
                min: 0
              }
            }
          ]
        }
      }
    };
  }

  componentDidMount() {
    this.setGovData().then(() => this.setChart());
  }

  setGovData = () => {
    return fetch(
      `https://bdl.stat.gov.pl/api/v1/data/by-variable/${this.state.id}`
    )
      .then(response => response.json())
      .then(result => this.setState({ govData: result }));
  };

  setChart = () => {
    let values = this.actualValues();
    this.setState(prevState => {
      prevState.dataChart.labels = values.map(x => x.year);
      prevState.dataChart.datasets[0].data = values.map(x => x.val);

      return prevState;
    });
  };

  actualValues() {
    if (!this.state.govData) {
      return [];
    }
    return this.state.govData.results.find(r => r.id === "000000000000").values;
  }

  actualState = () => {
    let values = this.actualValues();
    if (values.length <= 0) {
      return 0;
    }
    return values[values.length - 1];
  };

  render() {
    return (
      <>
        <h4 className='population-header'>Produkcja jaj</h4>
        <h6 className='population-info'>Ile jaj znoszą polskie kury?</h6>
        <p>{this.actualState().year}</p>
        <LiveCounter
          unit='sztuk'
          zeros='000'
          totalCount={this.actualState().val}
        />
        <div className='chart-container'>
          <Line data={this.state.dataChart} options={this.state.optionsChart} />
        </div>
      </>
    );
  }
}

export default EggsProduction;
