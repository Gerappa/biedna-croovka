import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import HomePage from "../HomePage/HomePage";
import AnimalsProduction from "../FoodProduction/AnimalsProduction/AnimalsProduction";
import EggsProduction from "../FoodProduction/EggsProduction/EggsProduction";
import MilkProduction from "../FoodProduction/MilkProduction/MilkProduction";
import Population from "../Population/Population";
import "./AppRouter.css";
import TopNav from "../TopNav/TopNav";
import SideNav from "../SideNav/SideNav";

class AppRouter extends Component {
  constructor() {
    super();
    this.state = {
      navIsOpen: false,
      navLinks: [
        {
          text: "Strona główna",
          component: HomePage,
          path: "/",
          selected: true
        },
        {
          text: "Populacja zwierząt",
          component: Population,
          path: "/population",
          selected: false
        },
        {
          text: "Produkcja żywca zwierzęcego",
          component: AnimalsProduction,
          path: "/production/animals",
          selected: false
        },
        {
          text: "Produkcja mleka",
          component: MilkProduction,
          path: "/production/milk",
          selected: false
        },
        {
          text: "Produkcja jaj",
          component: EggsProduction,
          path: "/production/eggs",
          selected: false
        }
      ]
    };
  }

  toggleSidebar = () => {
    this.setState({ navIsOpen: !this.state.navIsOpen });
  };

  handleLinkOnClick = event => {
    this.setState(prevState => {
      if (prevState.listItem.some(x => x.selected)) {
        console.log("prevState :", prevState);
        prevState.listItem.find(x => x.selected === true).selected = false;
      }
      console.log("window.location :", window.location.pathname);
      if (prevState.listItem.some(x => x.path === window.location.pathname)) {
        prevState.listItem.find(
          x => x.path === window.location.pathname
        ).selected = true;
      }
      return prevState;
    });
    this.closeNav();
  };

  render() {
    return (
      <Router>
        <TopNav openSidebar={this.toggleSidebar}></TopNav>
        <SideNav
          closeSidebar={this.toggleSidebar}
          navIsOpen={this.state.navIsOpen}
          navLinks={this.state.navLinks}></SideNav>
        <Switch>
          {this.state.navLinks.map(item => {
            const DynamicTag = item.component;
            return (
              <Route exact path={item.path} key={item.path}>
                <div style={{ position: "relative" }}>
                  <DynamicTag />
                </div>
              </Route>
            );
          })}
        </Switch>
      </Router>
    );
  }
}

export default AppRouter;
