import React, { Component } from "react";

class LiveCounter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      count: 0
    };
  }

  componentDidUpdate(prevProps) {
    if (this.props.totalCount !== prevProps.totalCount) {
      this.liveCount();
    }
  }

  liveCount = () => {
    let modulo = this.props.totalCount > 100000000 ? 10000 : 1000;
    for (let index = modulo; index < this.props.totalCount; index++) {
      if (index % modulo === 0) {
        setTimeout(() => this.setState({ count: index }), 1);
      }
    }
    setTimeout(() => this.setState({ count: this.props.totalCount }), 1);
  };

  render() {
    return (
      <h2 style={{ margin: "20px" }}>
        {this.state.count.toLocaleString()} {this.props.zeros} {this.props.unit}
      </h2>
    );
  }
}

export default LiveCounter;
