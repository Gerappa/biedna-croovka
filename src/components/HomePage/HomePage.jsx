import React, { Component } from "react";
import { Link } from "react-router-dom";

import "./HomePage.css";

class HomePage extends Component {
  render() {
    return (
      <>
        <div className='motto-card'>
          <h5 className='motto'>
            Statystyka
            <br />
            hodowli i uboju
            <br />
            zwierząt w Polsce
            {/* <div>
            <hr className='line' />
          </div> */}
          </h5>
          <div>
            <Link to='/population'>
              <button className='btn-learn-more'>Dowiedz się więcej</button>
            </Link>
          </div>
          {/* <h5 className='motto'>
          Codzienność budują nasze nawyki,
          <br /> które utrwalamy nieświadomie...
        </h5> */}

          {/* <div className='motto'>
          Ile zwierząt hoduje Polska?
          <br />
          Ile to kilogramów mięsa?
          <br />
          Ile litrów mleka dają nam krowy...
          <br />
          a ile sztuk jaj dają nam kury?
          <br />
          Sprawdź czy masz na to jakiś wpływ!
          <br />
        </div> */}
          {/* <div className='information'>
          <p>
            Dowiedz się ile zwierząt jest hodowanych w Polsce, ile produkuje się
            wołowiny, drobiu czy wieprzowiny. Dodatkwowo zamieszczono tu
            informacje o produktach odzwierzęcych(mleko i jaja).
            <br />
            Wszystkie dane pochodzą z{" "}
            <a href='https://stat.gov.pl/'>Głównego urzędu statystycznego</a>.
            <br />
            Jeśli interesuję Cię los tych zwierząt możesz użyć kalkulatora aby
            dowiedzieć się ile zwierząt zaoszczędzi życie jeśli Ty zmienisz
            diete.
          </p>
        </div> */}
        </div>
        <div className='quote'>
          Codzienność budują nasze nawyki, które utrwalamy nieświadomie...
        </div>
        <div className='more-information'>
          Dowiedz się ile zwierząt jest hodowanych w Polsce, ile produkuje się
          wołowiny, drobiu czy wieprzowiny. Dodatkwowo zamieszczono tu
          informacje o produktach odzwierzęcych (mleko + jaja).
        </div>
      </>
    );
  }
}

export default HomePage;
