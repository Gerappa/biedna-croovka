import React, { Component } from "react";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "./SideNav.css";

class SideNav extends Component {
  render() {
    let sidenavWidth = this.props.navIsOpen
      ? window.innerWidth > 768
        ? "20vw"
        : "60vw"
      : "0";
    return (
      <>
        <div className='sidenav' style={{ width: sidenavWidth }}>
          <p className='arrow-left' onClick={this.props.closeSidebar}>
            <FontAwesomeIcon icon='arrow-left' />
          </p>
          {this.props.navLinks.map(({ text, path, selected }) => (
            <Link
              to={path}
              key={path}
              className={selected ? "selected" : ""}
              onClick={this.props.closeSidebar}
              name={path}>
              {text}
            </Link>
          ))}
        </div>
        <div
          onClick={this.props.closeSidebar}
          className='menu-shadow'
          style={{ width: this.props.navIsOpen ? "100vw" : "0" }}></div>
      </>
    );
  }
}

export default SideNav;
