import React, { Component } from "react";
import "./SelectAnimals.css";

class SelectAnimals extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ulStyle: { gridTemplateColumns: "auto auto auto" }
    };
  }

  componentDidMount() {
    if (this.props.columns) {
      let columns = Array.apply("", { length: this.props.columns }).map(
        x => "auto"
      );
      this.setState({ ulStyle: { gridTemplateColumns: columns.join(" ") } });
    }
  }

  handleChange = event => {
    this.props.onSelected(event.target.value);
  };

  render() {
    return (
      <>
        <div className='select-group'>
          {this.props.items.map(item => {
            return (
              <button
                className={item.selected ? "select-btn selected" : "select-btn"}
                key={item.value}
                value={item.value}
                name={this.props.selectName}
                onClick={this.handleChange}>
                {item.text}
              </button>
            );
          })}
        </div>
        <h3 className='selected-text'>
          {this.props.items.find(x => x.selected).text}
        </h3>
      </>
    );
  }
}

export default SelectAnimals;
