import React, { Component } from "react";
import SelectAnimals from "../SelectAnimals/SelectAnimals";
import { Line } from "react-chartjs-2";

import "./Population.css";
import LiveCounter from "../LiveCounter/LiveCounter";

class Population extends Component {
  constructor(props) {
    super(props);
    this.state = {
      animals: [
        {
          id: 64051,
          value: "cattle",
          text: "Bydło",
          selected: true,
          govData: null
        },
        {
          id: 64119,
          value: "poultry",
          text: "Drób",
          selected: false,
          govData: null
        },
        {
          id: 461587,
          value: "pork",
          text: "Świnie",
          selected: false,
          govData: null
        }
      ],
      dataChart: {
        labels: [],
        datasets: [
          {
            label: "Populacja",
            backgroundColor: "rgb(99, 99, 99)",
            borderColor: "rgb(199, 199, 199)",
            data: []
          }
        ]
      },
      optionsChart: {
        scales: {
          yAxes: [
            {
              ticks: {
                min: 5000000
              }
            }
          ]
        }
      }
    };
  }

  componentDidMount() {
    this.setGovData().then(() => this.setChart());
  }

  setGovData = async () => {
    let actions = this.state.animals.map(x => x.id).map(this.apiRequest);
    await Promise.all(actions)
      .then(data =>
        data.forEach(govItem =>
          this.setState(prevState => {
            prevState.animals.find(
              x => x.id === govItem.variableId
            ).govData = govItem;
            return prevState;
          })
        )
      )
      .catch(err => {
        console.log(err);
      });
  };

  apiRequest = id =>
    fetch(
      `https://bdl.stat.gov.pl/api/v1/data/by-variable/${id}`
    ).then(response => response.json());

  setChart = () => {
    let values = this.actualValues();
    this.setState(prevState => {
      prevState.dataChart.labels = values.map(x => x.year);
      prevState.dataChart.datasets[0].data = values.map(x => x.val);

      return prevState;
    });
  };

  handleOnSelected = selected => {
    this.setState(prevState => {
      prevState.animals.find(
        animal => animal.selected === true
      ).selected = false;
      prevState.animals.find(
        animal => animal.value === selected
      ).selected = true;
      return prevState;
    }, this.setChart);
  };

  actualValues = () => {
    let govData = this.state.animals.find(animal => animal.selected).govData;
    if (!govData) {
      return [];
    }
    return govData.results.find(r => r.id === "000000000000").values;
  };

  actualState = () => {
    let values = this.actualValues();
    if (values.length <= 0) {
      return 0;
    }
    return values[values.length - 1];
  };

  render() {
    return (
      <>
        <h4 className='population-header'>Populacja</h4>
        <h6 className='population-info'>
          Jaka liczba zwierząt obecnie jest hodowana w Polsce?
        </h6>
        <SelectAnimals
          items={this.state.animals}
          onSelected={this.handleOnSelected}
        />
        <p>{this.actualState().year}</p>

        <LiveCounter unit='sztuk' totalCount={this.actualState().val} />

        <div className='chart-container'>
          <Line data={this.state.dataChart} options={this.state.optionsChart} />
        </div>
      </>
    );
  }
}

export default Population;
