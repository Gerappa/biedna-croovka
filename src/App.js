import React from "react";
import "./App.css";
import AppRouter from "./components/_AppRouter/AppRouter";
import { library } from "@fortawesome/fontawesome-svg-core";
import { fab } from "@fortawesome/free-brands-svg-icons";
import { faBars, faArrowLeft } from "@fortawesome/free-solid-svg-icons";

library.add(fab, faBars, faArrowLeft);

function App() {
  return (
    <>
      <div className='bg-container'>
        <div id='bg-main'></div>
        <div id='bg-square'></div>
        <div id='bg-square2'></div>
      </div>
      <div className='app'>
        <AppRouter />
      </div>
    </>
  );
}

export default App;
